# soal-shift-sisop-modul-3-ITB06-2022

# Soal 1

Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

b. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

c. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

d. Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

e. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

# Jawaban
a. Untuk melakukan unzip pada setiap file, dibuatlah suatu function "unzip_file" untuk melakukan unzip menggunakan command "unzip" yang disusul dengan nama file yang akan diunzip

```c++
void unzip_file(char name_file[1000]) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        char *argv[] = {"unzip","-q", name_file, NULL};
        execv("/bin/unzip", argv);
        printf("BERHASIL UNZIP\n");
        exit(EXIT_SUCCESS);

    } else {
        wait(NULL);
    }
}

```

Function tersebut akan dimasukan ke dalam thread agar kedua unzip file tersebu dapat dijalankan bersama. Berikut merupakan thread yangh dimasukkan function unzip_file

```c++
pthread_t tid_unzip[2];
void *thread_unzip(void *arg) {
    pthread_t id=pthread_self();

    if(pthread_equal(id,tid_unzip[0])) {
        unzip_file("music.zip");
    } else if(pthread_equal(id,tid_unzip[1])) {
        unzip_file("quote.zip");
    }

    return NULL;
}
```

Setelah dibuat thread, terapkan thread untuk melakukan unzip file tersebut ke dalam int main akan dapat dijalankan

```c++
	while(i < 2) {
		err1 = pthread_create(&(tid_unzip[i]),NULL,&thread_unzip,NULL); 
		if(err1!=0) {
			printf("\n can't create thread : [%s]",strerror(err1));
		} else {
			printf("\n create thread success\n");
		}

		i++;
	}

    pthread_join(tid_unzip[0],NULL);
	pthread_join(tid_unzip[1],NULL);
```

b. Sebelum menjalankan program untuk poin 1b, perlu dilakukan pembuatan file music.txt dan quote.txt untuk menyimpan hasil decode dari setiap file unzip. Berikut merupakan function untuk melakukan pembuatan file

```c++
void create_file(char *name) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        char *argv[] = {"touch", name, NULL};
        execv("/bin/touch", argv);
        
    } else {
        wait(NULL);
    }
}

```
Pembuatan file tersebut menggunakan command touch lalu disusul dengan nama file yang diharapkan.

Untuk melakukan decode text dari setiap file hasil unzip dan dimasukkan ke dalam file music.txt atau quote.txt, perlu dilakukan iterasi terlebih dahulu untuk melakukan pengambilan data string dari setiap file hasil unzip. Semua ini dilakukan dalam thread "tid_create_file". Berikut merupakan coding dari iterasi untuk hasil unzip music

```c++
create_file("music.txt");
        FILE* ptr, *fp;
        char str[50];
        char word[100];
        char files[100][100];
        strcpy(files[0], "m1.txt");
        strcpy(files[1], "m2.txt");
        strcpy(files[2], "m3.txt");
        strcpy(files[3], "m4.txt");
        strcpy(files[4], "m5.txt");
        strcpy(files[5], "m6.txt");
        strcpy(files[6], "m7.txt");
        strcpy(files[7], "m8.txt");
        strcpy(files[8], "m9.txt");

        for(int i = 0; i<9; ++i) {
            FILE* ptr, *fp;
            char str[50];
            ptr = fopen(files[i], "r");
            fp = fopen("music.txt", "a");
            
            if (NULL == ptr) {
                    printf("file can't be opened \n");
            }
                
                while (fgets(str, 100, ptr) != NULL) {
                    fprintf(fp, "%s\n", base64_decode(str));
                    fclose(fp);
                }    
                fclose(ptr);    
            }
```
Berikut iterasi untuk hasil unzip dari quote

```c++
 create_file("quote.txt");
        FILE* ptr, *fp;
        char str[50];
        char word[100];
        char files[100][100];
        strcpy(files[0], "q1.txt");
        strcpy(files[1], "q2.txt");
        strcpy(files[2], "q3.txt");
        strcpy(files[3], "q4.txt");
        strcpy(files[4], "q5.txt");
        strcpy(files[5], "q6.txt");
        strcpy(files[6], "q7.txt");
        strcpy(files[7], "q8.txt");
        strcpy(files[8], "q9.txt");

        for(int i = 0; i<9; ++i) {
            FILE* ptr, *fp;
            char str[50];
            ptr = fopen(files[i], "r");
            fp = fopen("quote.txt", "a");
            
            if (NULL == ptr) {
                    printf("file can't be opened \n");
                }
                
                while (fgets(str, 100, ptr) != NULL) {
                    fprintf(fp, "%s\n", base64_decode(str));
                    fclose(fp);
                }    

                fclose(ptr);    
            }

```

Setiap hasil dari iterasi tersebut, akan dilakukan decode menggunakan base64_decode sebelum hasilnya dimasukkan ke dalam file yang sesuai (music.txt untuk hasil unzip music.zip dan quote.txt untuk hasil unzip quote.zip). Berikut coding untuk mengdecode tiap text yang berada pada setiap file

```c++
char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* base64_decode(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(cipher) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';
    return plain;
}
```

Setelah berhasil dilakukan iterasi dan decode setiap string dalam file, maka hasil decode tersebut akan masuk ke dalam file yang sesuai (file yang diawali dengan q, akan dimasukkan ke dalam quote.text, file yang diawali dengan m, akan dimasukkan ke dalam music.txt)

c. Sebelum memindahkan file music.txt dan quote.txt, dibuat folder hasil terlebih dahulu menggunakan command mkdir, berikut merupakan codingan untuk membuat folder


```c++
pid_t child_id;
    child_id = fork();
    
    if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_hasil[] = {"mkdir", "-p", "hasil", NULL};
        execv("/bin/mkdir", argv_hasil);
    } else {
        wait(NULL);
    }
```
Setelah dilakukan pembuatan folder, file music.txt dan quote.txt dipindahkan ke dalam folder hasil menggunakan coding seperti berikut

```c++
     if(child_id < 0) exit(EXIT_FAILURE);

        if(child_id == 0) {
            char *argv_music[] = {"mv", "music.txt", "hasil/music.txt", NULL};
            execv("/bin/mv", argv_music);

        } else {
            wait(NULL);
        }
```

```c++
 if(child_id < 0) exit(EXIT_FAILURE);

        if(child_id == 0) {
            char *argv_quote[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
            execv("/bin/mv", argv_quote);

        } else {
            wait(NULL);
        }
```
d. Untuk mendapatkan nama user sebelum digabungkan dengan kata-kata "'mihinomenest", digunakan function `getlogin()`.Untuk melakukan penggabungan "'mihinomenest" dan data getlogin() yang berupa string, dilakukan concat seperti berikut

```c++
    char zip_password[1000];
    strcpy(zip_password, "minihomenest");
    strcat(zip_password, getlogin());
```

Setelah didapatkan password, lakukan zip pada folder hasil menggunakan command `zip` yang disusul dengan `-qrP`, password yang diinginkan, folder yang ingin di zip, dan nama file untuk zipnya

```c++
pid_t child_id_1;
    child_id_1 = fork();

    if(child_id_1 < 0) exit(EXIT_FAILURE);

    if(child_id_1 == 0) {
        char *argv_password_zip[] = {"zip", "-qrP", zip_password, "hasil.zip", "hasil", NULL};
        execv("/bin/zip", argv_password_zip);
    } else {
        wait(NULL);
    }
```

e. Untuk proses pertama, yaitu unzip pada file hasil.zip. Sebelum melakukan unzip, perlu didapatkan password untuk melakukan unzip, yaitu dengan menggunakan cara seperti berikut

```c++
        char zip_password[1000];
        strcpy(zip_password, "minihomenest");
        strcat(zip_password, getlogin());
```

Setelah didapatkan passwordnya, dilakukanlah unzip untuk file hasil.zip menggunakan command "unzip" 

```c++
 if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            char *argv_unzip[] = {"unzip", "-qo", "-P", zip_password, "hasil.zip", NULL};
            execv("/bin/unzip", argv_unzip);
        } else {
            wait(NULL);
        }
```

Untuk proses yang kedua, pertama dipanggil function create_file untuk melakukan pembuatan file "no.txt". Setelah itu, open file tersebut dan masukkan text "No" ke dalam file tersebut. Berikut adalah penerapan perintah tersebut dalam coding

```c++
child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            create_file("no.txt");
            FILE* fp = fopen("no.txt", "a");
            fprintf(fp, "No");
            fclose(fp);
    }
```

Berikut adalah gabungan dari program unzip file dan pembuatan file "n0.txt" dalam thread

```c++
pthread_t tid_unzip_no_file[2];
void * unzip_no_file(void *arg) {
    pthread_t id=pthread_self();
    pid_t child_id;
    child_id = fork();

    if(pthread_equal(id,tid_unzip_no_file[0])) {
        char zip_password[1000];
        strcpy(zip_password, "minihomenest");
        strcat(zip_password, getlogin());
        if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            char *argv_unzip[] = {"unzip", "-qo", "-P", zip_password, "hasil.zip", NULL};
            execv("/bin/unzip", argv_unzip);
        } else {
            wait(NULL);
        }
    } else if(pthread_equal(id,tid_unzip_no_file[1])) {
        child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            create_file("no.txt");
            FILE* fp = fopen("no.txt", "a");
            fprintf(fp, "No");
            fclose(fp);
        }
    }
}
```

Untuk menjalankan thread tersebut, berikut penerapan thread dalam int main

```c++
while(k < 2) {
		err3 = pthread_create(&(tid_unzip_no_file[k]),NULL,&unzip_no_file,NULL); 
		if(err3!=0) {
			printf("\n can't create thread : [%s]",strerror(err3));
            
		} else {
			printf("\n create thread success\n");
		}

		k++;
	}

    pthread_join(tid_unzip_no_file[0],NULL);
	pthread_join(tid_unzip_no_file[1],NULL);
```

Setelah thred tersebut berjalan, program tersebut disusul dengan memasukkan file no.txt kedalam folder hasil untuk dilakukan zip kembali menggunakan aturan zip sebelumnya, yaitu menggunakan password yang sudah ditentukan sebelumnya

```c++
child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_zip[] = {"zip", "-qrP", zip_password, "hasil.zip", "./hasil", "./no.txt", NULL};
        execv("/bin/zip", argv_zip);
    }
```

## Soal 3

a. Praktikan diminta untuk mengextract zip yang sudah didownload dan membuat directory berdasarkan extensi file yang ada pada zip yang telah di extract, dan akan di taruh pada “/home/[user]/shift3/, dan working directory pada “/home/[user]/shift3/hartakarun/”.

b. Kemudian praktikan diminta untuk memasukan file yang sudah diextract dengan folder yang sesuai dengan extensinya.

c. Kemudian praktikan diminta untuk setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

d. Kemudian Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.

e. Client dapat mengirimkan file “hartakarun.zip” ke server

###Jawaban

a. Praktikan hanya bisa melakukan pembuatan directory menggunakan `mkdir()` dan akan di increment sebanyak jumlah extensi file yang ada.

```c
const char *direktori[] = {"jpg", "tar.gz", "c", "png", "txt", "zip", "gif", "pdf", "jpeg", "sh", "hex", "bin", "Unknown", "Hidden", "js", "gns3project", "fbx", };

int main(void) {
    int i = 0;
    while (i <= 17) {
       mkdir(direktori[i], S_IRWXU);
       i++;
    }
```
